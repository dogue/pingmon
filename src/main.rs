use chrono::prelude::*;
use std::process::Command;
use std::time::{Duration, Instant};
use structopt::StructOpt;

fn main() {
    let args = pingmon::Opts::from_args();

    // Initialize the terminal (and file) logging facilities
    pingmon::init_log(&args);

    log::info!("Starting ping monitor...");

    let start_time = Instant::now();
    let mut sequence: u64 = 0;

    // For each iteration of the loop, ping is run as an external command against
    // the specified host. The exit status of ping is checked after each run.
    //
    // If the exit code is 1 (no reply), we break the loop and return the elapsed time
    // for which the loop ran.
    //
    // If the exit code is 2 (any other error), we issue an error to the log and exit.
    // We can't do anything about that.
    //
    // Otherwise if the exit code is 0, we log some data for verbose mode, increase the
    // packet counter, then sleep for however many seconds the user chose as the interval.
    let elapsed_seconds = loop {
        let output = Command::new("sh")
            .arg("-c")
            .arg(format!("ping -c 1 -W 5 {}", args.host))
            .output()
            .expect("Failed to execute ping command");

        if output.status.code().unwrap() == 1 {
            log::info!("Received non-zero exit status");
            break start_time.elapsed().as_secs();
        } else if output.status.code().unwrap() == 2 {
            log::error!("Unknown error - ping exited with status code (2)");
            break 0;
        }

        if args.verbose >= 1 {
            log::info!("Elapsed time (seconds): {}", start_time.elapsed().as_secs());
        }
        if args.verbose >= 2 {
            log::info!("Packet #: {}", sequence);
        }
        if args.verbose >= 3 {
            log::info!("Exit status: {}", output.status.code().unwrap());
        }

        sequence += 1;

        std::thread::sleep(Duration::from_secs(args.interval));
    };

    // Elapsed seconds will only ever be zero if ping exited with status (2), so exit.
    if elapsed_seconds == 0 {
        std::process::exit(1);
    }

    // Parse the elapsed seconds into an hour:minute:second format for human eye orbs
    let elapsed_time = NaiveTime::from_hms(0, 0, elapsed_seconds as u32);

    log::info!("Total elapsed time: {}", elapsed_time.to_string());
}
