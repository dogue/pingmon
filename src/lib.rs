use simplelog::*;
use std::fs::File;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "pingmon", version = env!("CARGO_PKG_VERSION"))]
pub struct Opts {
    #[structopt(short, long, parse(from_occurrences))]
    pub verbose: u8,

    #[structopt(
        short,
        long,
        default_value = "30",
        help = "Seconds to wait between pings"
    )]
    pub interval: u64,

    #[structopt(short, long, parse(from_os_str))]
    pub outfile: Option<PathBuf>,

    pub host: String,
}

pub fn init_log(args: &Opts) {
    let log_level = LevelFilter::Info;

    let config: Config = ConfigBuilder::new().set_time_to_local(true).build();

    // Create a Vec of loggers with a TermLogger
    let mut loggers: Vec<Box<dyn SharedLogger>> = vec![TermLogger::new(
        log_level,
        config.clone(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )];

    // If outfile is supplied by the user, push a WriteLogger to the Vec
    if let Some(outfile) = &args.outfile {
        loggers.push(WriteLogger::new(
            log_level,
            config,
            File::create(outfile).unwrap(),
        ));
    };

    // Init the logging system with the Vec of loggers
    CombinedLogger::init(loggers).unwrap();
}
